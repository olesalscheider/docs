Title: Contributing
CSS: /css/main.css

#{include head}

Contributing Documentation
==========================
- See [updating and writing documentation](documentation.html)

Contributing Code
=================

* This will become a table of contents (this text will be scraped).
{:toc}

## Overview

Exherbo uses [git](http://git-scm.com) for source code management (SCM).
Some of the guidelines outlined in this document are based upon `Documentation/SubmittingPatches`,
as part of [git.git](http://repo.or.cz/w/git.git?a=blob;f=Documentation/SubmittingPatches)

## Style requirements

- Configure your git author name and email properly. Use your real name.
  See [`man git-config`](http://schacon.github.com/git/git-config.html), look for
  `user.name` and `user.email`.

- Generate your patches using `git format-patch`, from local commits. If your patch renames files,
  please use the options `-M -C -C` to `git format-patch`, to detect copies and renames. This makes
  the resulting patch much easier to read. Alternatively, you can enable `-M -C` by default by
  running `git config --global diff.renames copy`.  See [`man
  git-format-patch`](http://schacon.github.com/git/git-format-patch.html).

- Use a `clear and descriptive commit message`, explaining the change.

  - Write a `short one line summary`, followed by an `empty line`, and, as you see fit, a more
    `elaborate description` of your changes

  - `Version bump` is not an acceptable commit message. You should at least mention the package and
    version you're adding. Try running `git log --oneline` on a repository full of `Version bump`
    commits and you'll understand why.

  - In case you're making non-obvious changes, consider explaining the reasoning behind the changes.

  - If you rearrange code or make white space and style changes then avoid making real changes in
    the same commit. Put the actual changes you do in a separate commit. This makes it easier to
    review as well as allowing git to track the history of the affected code properly.

  - Do make references to relevant bug reports, mailing lists, ... where appropriate.

- Exhereses use 4 spaces and strictly no tabs for indentation. Run your patch through `git diff
  --check`, to verify that it doesn't introduce whitespace errors.

- `app-vim/exheres-syntax` and `app-emacs/exheres-mode` will highlight common exheres syntax errors. Use them.

## Submitting patches for package repositories

- When submitting a new package, first consider whether it's really important enough to be in the
  official repositories, or whether you should instead put it in your own supplemental repository.
  If the quality of your supplemental repository is reasonably high, host it somewhere 
  (preferably on our [Gitlab instance](//gitlab.exherbo.org/exherbo)), and
  we'll consider adding it to the unavailable-unofficial repository.

- Do submit original work. Don't blindly copy ebuilds, adjusting for differences between ebuilds
  and exhereses. If you need to install a package, there is a tool far more suitable that you can
  use instead of blindly adapting ebuilds to exheres.

- Don't try to bump large and critical packages (e.g gcc, X, grub...) on your own without speaking
  to their maintainers. Bumping such packages takes some time and requires extra testing, so
  please be patient.

- Sometimes patching upstream sources is unavoidable. If you do apply patches, make sure they're
  properly documented according to our patches policy, found in
  [exheres-for-smarties](eapi/exheres-for-smarties.html). Always consider that adding
  patches that will not be accepted upstream means you get to maintain those.

- To be included in any of our repositories, contributions must be licenced under the GPL-2. Add an
  appropriate copyright notice if your contributions to a given file warrant it.
  See [exheres-for-smarties](eapi/exheres-for-smarties.html).

- Patches should be contributed via our [Gitlab](//gitlab.exherbo.org/exherbo).
  See our [Gitlab docs](/docs/gitlab.html) for more information.

  Other (discouraged) options include:

  - our IRC channel [#exherbo](irc://irc.freenode.org/#exherbo), [using the patchbot](patchbot.html)

- If a package should be moved from one repository to another, it's important to also import any
  prior package related git actions (history) of that package. This is because commit messages often explain
  why certain changes have been made. Also, the history itself is of interest. To do this you have to first extract
  the history in patch format and import those patches via the usual procedure by using git-am.

      cd /path/to/current_repository
      git log --pretty=email --patch-with-stat --reverse -- path/to/package | (cd /path/to/new_repository && git am)

## Submitting your repository for inclusion in unavailable-unofficial

If you have your own repository and its quality is fairly high, you should submit it for review so
it will be included in unavailable-unofficial.
To do that, just add it to our infra-scripts git repository as in this [example](repository-add-example.txt)
and push it to Gitlab.

This way, others will not duplicate any work you may
have done, and you will get helpful tips on how to improve your packaging skills. Also, this will
allow others to more easily contribute to your packages or point out bugs. See kloeri's
[blog post](http://kloeri.livejournal.com/11205.html) if you are not yet convinced.

We expect a decent level of responsiveness from you and we reserve the right to remove repositories
with broken packages if you do not respond to patches and inquiries within a reasonable amount of
time (Rule of thumb: within 30 days).

More concretely, if there's a patch for your repository for 30 days or more, we will consider your
repository for removal from unavailable-unofficial.
You'll get up to two additonal "wake-up calls" before the actual removal, though. If you're not on
IRC, we'll send you an email to your last known address (so make sure to use a valid, working email
address in your exheres, etc.).

This is *not* about "punishing" you or something like that but to ensure that the repositories in
unavailable-unofficial are maintained according to Exherbo's quality standards.

Remember that it is trivial to have your repository re-added once it is fixed up/maintained properly
again.

### Exherbo hook

To have fancy IRC commit notifications, please consider using our push hook. We currently support
GitHub, Gitorious and self-hosted repositories. Ask any Exherbo developer on IRC to get the WebHook
URL.

## Caveats

- When testing documentation updates you should use the Makefiles in the git repository to generate
  the HTML. This makes sure you get the same results as on our servers. Make sure you have Maruku
  installed.

--
Copyright 2009 Ingmar Vanhassel

#{include CC_3.0_Attribution_ShareAlike}
#{include foot}

<!-- vim: set tw=100 ft=mkd spell spelllang=en sw=4 sts=4 et : -->
