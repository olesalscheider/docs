Title: <old package name> has been renamed to <new package name>
Author: <your real name> <<your email-address>>
Content-Type: text/plain
Posted: <creation date>
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: <old package name>

Please install <new package name> and *afterwards* uninstall <old package name>.

1. Take note of any packages depending on <old package name>:
cave resolve \!<old package name>

2. Install <new package name>:
cave resolve <new package name> -x

3. Re-install the packages from step 1.

4. Uninstall <old package name>
cave resolve \!<old package name> -x

Do it in *this* order or you'll potentially *break* your system.
