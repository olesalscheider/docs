Title: systemd
CSS: /css/main.css

#{include head}

Systemd
=======

* This will become a table of contents (this text will be scraped).
{:toc}

## Overview

This is directly lifted from the work of Wulf C. Krueger and can be found in its original form on
[his blog](//www.mailstation.de/wordpress/?p=48 "Philantrop's Blog - HowTo: systemd on Exherbo").
This will be updated to match changes there as possible.

[systemd](http://www.freedesktop.org/wiki/Software/systemd "systemd
homepage") is, at the time of this writing, the best maintained init
system available in Exherbo.


## Installing

### Kernel Configuration
You have to run a Linux kernel \>=3.13. For unified cgroup hierachy
support you'll need \>=4.2. The new kernel is only needed at runtime,
not for building systemd (Note: The current iptables (1.6.2) in our
stage tarballs requires \>=4.4). Kernel options for systemd: cf.
systemd's README, here's an excerpt:

    CONFIG_DEVTMPFS
    CONFIG_CGROUPS (it's OK to disable all controllers)
    CONFIG_INOTIFY_USER
    CONFIG_SIGNALFD
    CONFIG_TIMERFD
    CONFIG_EPOLL
    CONFIG_NET
    CONFIG_SYSFS
    CONFIG_PROC_FS
    CONFIG_FHANDLE (libudev, mount and bind mount handling)

Udev will fail to work with the legacy sysfs layout:

    CONFIG_SYSFS_DEPRECATED=n

Legacy hotplug slows down the system and confuses udev:

    CONFIG_UEVENT_HELPER_PATH=""

Userspace firmware loading is deprecated, will go away, and
sometimes causes problems:

    CONFIG_FW_LOADER_USER_HELPER=n

Some udev rules and virtualization detection relies on the following:

    CONFIG_DMIID


Support for some SCSI devices serial number retrieval, to
create additional symlinks in /dev/disk/ and /dev/tape:

    CONFIG_BLK_DEV_BSG

Optional but strongly recommended:

    CONFIG_IPV6
    CONFIG_AUTOFS4_FS
    CONFIG_TMPFS_POSIX_ACL
    CONFIG_TMPFS_XATTR
    CONFIG_SECCOMP

For systemd-bootchart, several proc debug interfaces are required:

    CONFIG_SCHEDSTATS
    CONFIG_SCHED_DEBUG

For UEFI systems:

    CONFIG_EFI_VARS
    CONFIG_EFI_PARTITION


These two are only used for readahead stuff which is not enabled by default:

    CONFIG_FANOTIFY=y
    CONFIG_FANOTIFY_ACCESS_PERMISSIONS=y

### Userland configuration

Once the kernel is properly configured and built,

- Set the "systemd" option globally in `/etc/paludis/options.conf`: `*/* systemd`
- Install systemd: `cave resolve -x sys-apps/systemd` (Read what cave tells you. If in doubt, read [Paludis' documentation](http://paludis.exherbo.org/).)
- Reinstall every package with the new option set: `cave resolve world -cx`
- Make sure systemd is selected as the init system: `eclectic init list` (and, if necessary `eclectic init set systemd`)
- Set the desired hostname in `/etc/hostname`
- Optional: Edit `/etc/vconsole.conf` to your liking. (If you delete it, empty it or comment out everything, systemd will use the kernel's defaults.)
- Optional: Edit `/etc/machine-info` to your liking.
- Read [Lennart's blog post about the other configuration files](http://0pointer.de/blog/projects/the-new-configuration-files).
- Install a Linux kernel \>=2.6.39. (see above for kernel options, etc.)
- Reboot

After that reboot, you'll be in a console with a minimal set of
services started, hopefully ready to log in. Log in as root (the
keyboard layout is set to US in vconsole.conf (see above) by
default!). Then you can enable whatever services (found in
/lib/systemd/system) you like, suggested ones are: `sshd.socket` and
`dhcpcd.service` or `NetworkManager.service`.

In the instance of troubles with sshd starting or missing keys, please
consult `man sshd`.


As an extremely simple and limited alternative to
NetworkManager.service, there's network.service and network.conf which
get installed if you set the "simple-net" option for
systemd. network.service only allows for static network setups with
IPv4.

Alternatively, you can use dhcpcd.service.

It is advisable to hold off on enabling your display manager's service
(either kdm.service, gdm.service, xdm.service or slim.service) until
your basic system has at least booted properly once and you can reach
your system using ssh because in case things go wrong, it's easier not
to have to wrestle with a GUI.

To actually enable a service, run `systemctl enable <foo.service>`
More details can be found in systemd's man page.

If you need help, it's available in #exherbo, as usual, but reading
this page **first** is **absolutely necessary**.

 

##FAQ

- **How/where do you specify extra modules to be loaded?**
  You put the module name into `/etc/modules-load.d/foo.conf` 
  and it will get loaded, unless systemd-udev has already loaded it for you.
  Check that first.

- **My hostname is set to something funny, e. g. '08′!** 
  If you're using NetworkManager, you need to set your hostname in `/etc/NetworkManager/NetworkManager.conf`, too.

- **I'm getting messages about failing services, e. g. dev-hugepages.mount or sys-kernel-debug.automount.
  What's up with that?**
  You can either enable the corresponding kernel options, delete the symlink
  (e.g. `/lib/systemd/system/basic.target.wants/sys-kernel-debug.automount`) or just ignore those messages.
  They're harmless.

- **When sshd.socket is enabled, every closed ssh connection leaves a failed service around,
  e.g. `sshd@192...:55140.service`.**
  Harmless as well. There are no resources used by those so ignore them. (This should be fixed anyway.)

- **Where can I learn more about the usual administration tasks?** 
  Read Lennart's series of blog posts about systemd for administrators:
  [Part 1](http://0pointer.de/blog/projects/systemd-for-admins-1.html),
  [Part 2](http://0pointer.de/blog/projects/systemd-for-admins-2.html),
  [Part 3](http://0pointer.de/blog/projects/systemd-for-admins-3.html),
  [Part 4](http://0pointer.de/blog/projects/systemd-for-admins-4.html),
  [Part 5](http://0pointer.de/blog/projects/three-levels-of-off.html),
  [Part 6](http://0pointer.de/blog/projects/changing-roots),
  [Part 7](http://0pointer.de/blog/projects/blame-game),
  [Part 8](http://0pointer.de/blog/projects/the-new-configuration-files)

- **How do I debug problems with systemd?**
  Read this page
  [http://fedoraproject.org/wiki/How_to_debug_Systemd_problems](//fedoraproject.org/wiki/How_to_debug_Systemd_problems)

- **I'm completely lost. What do I do?**
  Please remember there's always a friend around. It's called "man".

--
Copyright 2013 Wulf C. Krueger, A Frederick Christensen

#{include CC_3.0_Attribution_ShareAlike}
#{include foot}

<!-- vim: set tw=100 ft=mkd spell spelllang=en sw=4 sts=4 et : -->
